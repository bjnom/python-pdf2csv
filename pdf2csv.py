#!/usr/bin/python

#                __
#               / _)         
#        .-^^^-/ /          
#     __/ bjnom /              
#    <__.|_|-|_|   
#           
#	19 Sep 2020

# Instalar librerias linux debian
# sudo apt install python3-pip
# pip3 install tabula-py

# Librerias
import tabula
import datetime
import os

# Listado de archivos pdf
# Se pueden listar los archivos ejecutando $ find ./ -printf "\"%f\",\n"
archivos = [ "tabla1", "tabla2" ]

# Convertir PDF en CSV
for archivo in archivos:
    # para el registro de tiempo de ejecucion
	now = datetime.datetime.now()
    # archivo que se lee
	lectura = archivo
    # ruta del archivo
	base = os.path.abspath(os.getcwd())
    # guardar archivo resultante en carpeta csv 
	salida = base+"/csv/"+archivo+".csv"
	print("Convirtiendo "+salida+" Sep-"+now.strftime("%d %H:%M:%S"))

    # Convertir archivo 
    # Se asigna area de la tabla de datos (esquina sup izq y,x,alto tabla, ancho tabla) para quitar las cabeceras 
    # y se asigna 6 gb de memoria ram para archivos grandes
	tabula.convert_into(lectura, salida, area=(62, 20, 693, 1060), output_format="csv",  java_options='-Xmx6144m', pages='all')

# Se concatenan archivos
os.system("cat "+base+"/csv/*.csv >> "+base+"/csv/total.csv")
